(defun part1 (input)
  (reduce '+ (loop for string in (split-sequence:split-sequence #\Newline input)
                for integer-list = (mapcar 'parse-integer (split-sequence:split-sequence #\Tab string))
                collect (apply 'max integer-list) into a
                collect (apply 'min integer-list) into b
                finally (return (mapcar '- a b)))))

#|
part 2:

5 9 2 8
9 4 7 3
3 8 6 5

|#

(defun read-input (input) 
  (loop for string in (split-sequence:split-sequence #\Newline input)
     collect (mapcar 'parse-integer (split-sequence:split-sequence #\Space string))))

(defun read-input-really (input)
  (loop for string in (split-sequence:split-sequence #\Newline input)
     collect (mapcar 'parse-integer (split-sequence:split-sequence #\Tab string))))

(defun has-two-zeroes-p (list)
  (let ((zeroes (remove-if-not 'zerop list)))
    (if (= (length zeroes) 2)
        T
        NIL)))

(defun zerop-01 (number)
  (if (zerop number)
      1
      0))

(defun get-two-zeroes (list-list)
  (loop for list in list-list
     when (has-two-zeroes-p list)
     return (mapcar 'zerop-01 list)))

(defun part2 (in)
  (let ((x  (- (length (first in)) 1)))
   (reduce '+ (mapcar (lambda (two-list)
;                        (apply '/ two-list))
                       (loop for line in in
                          for sorted = (sort line '>)
                          for foo = (loop for i from 0 to x
                                       for s-i = (nth i sorted)
                                       collect (loop for j from 0 to x 
                                                  for s-j = (nth j sorted)
                                                  collect (mod s-i s-j)))
                          collect (remove-if 'zerop (mapcar '* sorted (get-two-zeroes foo))))))))
