#|
You're standing in a room with "digitization quarantine" written in LEDs along one wall. The only door is locked, but it includes a small interface. "Restricted Area - Strictly No Digitized Users Allowed."

It goes on to explain that you may only leave by solving a captcha to prove you're not a human. Apparently, you only get one millisecond to solve the captcha: too fast for a normal human, but it feels like hours to you.

The captcha requires you to review a sequence of digits (your puzzle input) and find the sum of all digits that match the next digit in the list. The list is circular, so the digit after the last digit is the first digit in the list.

For example:

    1122 produces a sum of 3 (1 + 2) because the first digit (1) matches the second digit and the third digit (2) matches the fourth digit.
    1111 produces 4 because each digit (all 1) matches the next.
    1234 produces 0 because no digit matches the next.
    91212129 produces 9 because the only digit that matches the next one is the last digit, 9.

|#

(defun tolist (string)
  (mapcar (lambda (char) (if #1=(digit-char-p char)
                             #1#
                             (error "not an integer: ~a~%" char)))
          (coerce string 'list)))

(defun day1 (string)
  (let* ((numbs (tolist string))
         (first (first numbs))
         (prev  0)
         (sum 0))
    (dolist (number numbs)
      (when (= number prev)
        (setf sum (+ number sum)))
      (setf prev number))
    (if (= (first (last numbs)) first)
        (setf sum (+ sum first)))))


(defun day1-pt2 (string)
  (let* ((numbs (tolist string))
         (n (length numbs))
         (half (/ n 2)))
    (format t "n ~a half ~a~%" n half)
    (loop for i from 1 to half
       for once  = (1- i)
       for twice = (1- (* i 2))
       do (format t "i= ~a twice= ~a" (nth once numbs) (nth twice numbs))
       when (= (nth once numbs)
               (nth twice numbs))
       summing i into sum
       do (format t "sum: ~a~%" sum)
       finally (return (* 2 sum)))))
