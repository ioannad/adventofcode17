#|
     DAY 10
     ......
     KNOT HASH
     ---------

begin with a list of numbers from 0 to 255, a current position which begins at 0 (the first element in the list), a skip size (which starts at 0), and a sequence of lengths (your puzzle input). Then, for each length:

    Reverse the order of that length of elements in the list, starting with the element at the current position.
    Move the current position forward by that length plus the skip size.
    Increase the skip size by one.

The list is circular; 
Lengths larger than the size of the list are invalid.

Once this process is complete, what is the result of multiplying the first two numbers in the list?

INPUT: 88,88,211,106,141,1,78,254,2,111,77,255,90,0,54,205
|#

(ql:quickload 'split-sequence)
(use-package :split-sequence)
(ql:quickload :alexandria)
(use-package :alexandria)

(defvar *list* (loop for i from 0 to 255 collect i))
(defvar *input* "88,88,211,106,141,1,78,254,2,111,77,255,90,0,54,205")

(defun parse-input (input)
  (mapcar 'parse-integer (split-sequence:split-sequence #\, input)))

;; Circular lists

(defun knot (list integer skip circle-length)
  (let ((left  (subseq list 0 integer))
        (right (subseq list integer)))
    (let ((clist (alexandria:circular-list (append (reverse left)
                                                   right))))
      (subseq (loop for elt in (loop for i from 1 to 64
                                  append (car clist))
                 repeat (+ integer skip circle-length)
                 collect elt)
              #1=(+ integer skip)
              (+ #1# circle-length)))))

(defun track-origin (original-start integer skip circle-length)
  (mod (- original-start (+ integer skip))
       circle-length))

(defun knot-hash (integer-list list circle-length)
  (let ((answer list)
        (original-start 0))
    (loop for integer in integer-list
       for skip from 0 to (1- (length integer-list))
       do (setf answer (knot answer integer skip circle-length))
       do (setf original-start (track-origin original-start
                                             integer
                                             skip
                                             circle-length)))
    (values answer original-start)))

(defun part1 ()
  (let ((integer-list (parse-input *input*)))
    (multiple-value-bind (answer original-start)
        (knot-hash integer-list *list* 256)
    (* (nth original-start answer)
       (nth (1+ original-start) answer)))))

(assert (= (part1) 11375))

;; 28056 too high
;; 11375 just right.

#|
    PART 2
    
    your input should be taken not as a list of numbers, but as a string of bytes instead. Unless otherwise specified, convert characters to bytes using their ASCII codes

    Once you have determined the sequence of lengths to use, add the following lengths to the end of the sequence: 17, 31, 73, 47, 23.

    Second, instead of merely running one round like you did above, run a total of 64 rounds, using the same length sequence in each round. 
    The current position and skip size should be preserved between rounds.
    Once the rounds are complete, you will be left with the numbers from 0 to 255 in some order, called the sparse hash. Your next task is to reduce these to a list of only 16 numbers called the dense hash. To do this, use numeric bitwise XOR to combine each consecutive block of 16 numbers in the sparse hash (there are 16 such blocks in a list of 256 numbers).

    Finally, the standard way to represent a Knot Hash is as a single hexadecimal string; the final output is the dense hash in hexadecimal notation.
|#

(defun parse-input-2 (input-string)
  (mapcar 'char-code (coerce input-string 'list)))

(defun make-input (ascii-codes)
  (append ascii-codes '(17 31 73 47 23)))

(assert (equal (make-input (parse-input-2 "1,2,3"))
               '(49 44 50 44 51 17 31 73 47 23)))

;;; I use 'ascii-codes for 'length-sequence.

(defun sparse-hash (list ascii-codes)
  (let ((ascii-codes-2 (loop for i from 1 to 64 
                          append ascii-codes)))
    (multiple-value-bind (answer original-start)
        (knot-hash ascii-codes-2 list 256)
      (subseq (append answer answer)
              original-start 
              (+ original-start (length list))))))
         
(defun dense-hash (list ascii-codes)
  (let ((sparse-hash (sparse-hash list ascii-codes)))
    (loop for i from 0 to 240 by 16
       for chunk = (subseq sparse-hash i (+ i 16))
       collect (apply 'logxor chunk))))

(defun ascii-list-to-hex-string (ascii-list)
  (let ((hex-list (mapcar (lambda (ascii-code) 
                            (if (< ascii-code 16)
                                (concatenate 'string "0"
                                             (write-to-string ascii-code 
                                                              :base 16))
                                (write-to-string ascii-code :base 16)))
                          ascii-list)))
    (format nil "~{~a~}" hex-list)))

(assert (string-equal (ascii-list-to-hex-string '(64 7 255))
                      (string-upcase "4007ff")))

(defun part2 (input)
  (ascii-list-to-hex-string
   (dense-hash *list* (make-input (parse-input-2 input)))))
               
(print (part2 *input*))
