#|
Day 3

Part 1

Each square on the grid is allocated in a spiral pattern starting at a location marked 1 and then counting up while spiraling outward. For example, the first few squares are allocated like this:

17  16  15  14  13
18   5   4   3  12
19   6   1   2  11
20   7   8   9  10
21  22  23---> ...

How many steps are required to carry the data from the square identified in your puzzle input all the way to the access port?

|#

(defun which-square (number) ; remember isqrt 
  (let ((ceil (ceiling (sqrt number))))
    (if (evenp ceil)
        (1+ ceil)
        ceil)))

(defun previous-square (number)
  (- (which-square number) 2))

(defun ^2 (number)
  (* number number))

(defun which-position (number previous-square)
  (- number (^2 previous-square)))

(defun square-radius (square)
  (/ (1- square) 2))

(defun get-corner-position (which-corner square)
  (let ((previous-square (- square 2)))
    (+ (* which-corner previous-square)
       which-corner)))

(defun next-corner (position previous-square &aux which-corner)
  (let ((answer (loop for a from 1 to 4
                   for next-corner-position = (get-corner-position 
                                               a (+ 2 previous-square))
                   do (setf which-corner a)
                   when (< position next-corner-position)
                   return next-corner-position)))
 ;   (format t "position=~a, next-corner-position: ~a, which-corner: ~a~%" 
 ;           position answer which-corner)
    (if answer 
        (values answer which-corner)
        (values nil 5))))

(defun manhattan-distance (number)
  (let* ((square   (which-square number))
         (prev-sq  (- square 2)) ; x
         (pos      (which-position number prev-sq))
         (extra    (square-radius prev-sq)))
    (let ((next-corner (next-corner pos prev-sq)))
      (+ 1 extra (abs (- pos (- next-corner extra 1)))))))

;;; Correct answer: 430

#|
Part 2

store the value 1 in square 1. Then, in the same allocation order as shown above, they store the sum of the values in all adjacent squares, including diagonals.

What is the first value written that is larger than your puzzle input?

Your puzzle input is still 312051.
|#

; need the coordinates function?
; NO! I HAVE POSITION AND a, b!

(defun corner-p (position previous-square)
  (let ((it (next-corner (1- position) previous-square)))
    (if (= it position)
        T
        NIL)))

(defun one-before-corner-p (position next-corner)
  (= 1 (- next-corner position)))
  
(defun one-after-corner-p (position next-corner previous-square)
  (= (- next-corner position)
     previous-square))

(defun get-corner-value (which-corner square)
  (let ((previous-square (- square 2)))
    (+ (get-corner-position which-corner square)
       (^2 previous-square))))

;;; Ok adjacent is then always the previous number, and:
;;; if position is 1, position 1 of previous-square
;;; if position is 2, position 2 of previous-square
;;; if position is a corner c(sq), then the same corner of the previous-square
;;;                                so c(previous-square)
;;; if position is before a corner c(sq), then c(previous-square) and the number
;;;                                       before that
;;; if position is after a corner c(sq), then c(previous-square) and the number
;;;                                      after that
;;; if position is neither of the above, and its distance from the next square
;;;                                      c(sq) is d, then adjacent are:
;;;                                      c(sq)-d, c(sq)-d+1, and c(sq)-d+2
;;; here goes nothing
(defun adjacent-over-9 (number)
  "number = position + (^2 previous-square)."  
  (let* ((previous-square (previous-square number))
         (position        (which-position number previous-square)))
    (multiple-value-bind (next-corner which-corner)
        (next-corner position previous-square)
;      (format t "previous-square=~a, position=~a, next-corner=~a, which-corner=~a~%"
;              previous-square position next-corner which-corner)
      (append (list (1- number))
              (cond ((= position 1)
                     (list (1+ (^2 (- previous-square 2)))))
                    ((= position 2)
                     (list (1+ #3=(^2 (- previous-square 2)))
                           (+ 2 #3#)
                           (^2 previous-square)))
                    ((corner-p position previous-square)
                     (list (get-corner-value (1- which-corner) 
                                             previous-square)))
                    ((one-before-corner-p position next-corner)
                     (list #1=(get-corner-value which-corner 
                                                previous-square)
                           (1- #1#)))
                    ((one-after-corner-p position next-corner previous-square)
                     (list #2=(get-corner-value (1- which-corner) 
                                                previous-square)
                           (1+ #2#)
                           (- number 2)))
                    (T 
                     (let ((side (1+ (- (get-corner-value which-corner 
                                                          previous-square)
                                        (- next-corner position)))))
                       (list side (1+ side) (1- side)))))
              (when (or (= 5 which-corner)
                        (and (= 4 which-corner)
                             (one-before-corner-p position next-corner)))
                (list (1+ (^2 previous-square))))))))

;; This is WAY too complicated.
;; position on the outer square is NOT a good languange for this.
;; Whatever.

(defun adjacent-2-to-9 (number)
  (append (list 1 (1- number))
          (when (member number (list 4 6 8))
            (list (- number 2)))
          (when (member number (list 8 9))
            (list 2))))

(defun adjacent (number)
  (cond ((= number 1)  1)
        ((< number 10) (adjacent-2-to-9 number))
        (T             (adjacent-over-9 number))))

;; value from now on means day2 kinda value

(defun the-value (number)
  (case number
    (1 1) (2 1)
    (otherwise (reduce '+ (mapcar 'the-value (adjacent number))))))

(defun part2 (number)
  (let ((answer 1))
    (print (loop for n from 1 to number
              while (< answer number)
              do (setf answer (the-value n))
              finally (return n)))
    answer))
         
;;; 312833 too high

;;; 312453 correct answer  
