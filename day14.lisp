#|
   DAY 14
   ------
   Disk Defragmentation
   --------------------

   The disk in question consists of a 128x128 grid; each square of the grid is either free or used. On this disk, the state of the grid is tracked by the bits in a sequence of knot hashes.

   The hash inputs are a key string (your puzzle input), a dash, and a number from 0 to 127 corresponding to the row.

   The output of a knot hash is traditionally represented by 32 hexadecimal digits; each of these digits correspond to 4 bits, for a total of 4 * 32 = 128 bits. To convert to bits, turn each hexadecimal digit to its equivalent binary value, high-bit first

   Given your actual key string, how many squares are used?
   
   Your puzzle input is stpzcrnm.
|#

(load "~/code/adventofcode17/day10.lisp")
(setf *input* "stpzcrnm")

(defun get-disk (input) ; takes ages...... :( 
  (loop for i from 0 to 127
     for input-string = (concatenate 'string input "-" (write-to-string i))
     collect (part2 input-string)))

(defun to-4-bits (char)
  (let* ((stringify (coerce (list char) 'string))
         (hex (parse-integer stringify :radix 16)))
    (format nil "~4,'0d" (write-to-string hex :base 2))))

(defun hexs-to-bits (hex-string)
  (let ((4bits-list (mapcar 'to-4-bits (coerce hex-string 'list))))
    (apply 'concatenate 'string 4bits-list)))

(defun count-ones (bits-string)
  (loop for char in (coerce bits-string 'list)
     count (char-equal #\1 char)))

(defun get-fragmentation (input) ; takes aaaaaaaages. :( 
  (loop for hex-string in (get-disk input)
     summing (count-ones
              (hexs-to-bits hex-string))))

;;; 8114 too low
;;; 8250 correct
  
